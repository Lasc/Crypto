
/*
 * This file was used to test my RSA password encryption method 
 * that uses Modular Exponentiation.
 * I've coded the decryption to test if the key is obtained correctly
 * by using my own generated public and private key pairs
 * stored in files Public.key and Private.key
*/
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

public class DecryptAESKey {

	private static final String PUBLIC_KEY_FILE = "Public.key";
	private static final String PRIVATE_KEY_FILE = "Private.key";
	private static BigInteger publicModulus;
	private static BigInteger publicExponent;
	private static BigInteger privModulus;
	private static BigInteger privExponent;

	public static void main(String[] args) throws IOException {

		FileInputStream pubFile, privFile = null;
		ObjectInputStream pubStream, privStream = null;

		pubFile = new FileInputStream(new File(PUBLIC_KEY_FILE));
		pubStream = new ObjectInputStream(pubFile);

		privFile = new FileInputStream(new File(PRIVATE_KEY_FILE));
		privStream = new ObjectInputStream(privFile);

		try {
			publicModulus = (BigInteger) pubStream.readObject();
			publicExponent = (BigInteger) pubStream.readObject();

			privModulus = (BigInteger) privStream.readObject();
			privExponent = (BigInteger) privStream.readObject();

			System.out.println("Public Modulus in Hex: "
					+ publicModulus.toString(16));
			System.out
					.println("Public Modulus in BigInteger: "
							+ publicModulus);
			System.out.println("Public Exponent: " + publicExponent);

			System.out.println("My Private Modulus in Hex: "
					+ privModulus.toString(16));
			System.out
					.println("My Private Modulus in BigInteger: "
							+ privModulus);
			System.out.println("Private Exponent: " + privExponent);

			String parolaCriptata = encryptPassword("Mihai_Lasc_PaSsWoRd*2015");
			System.out.println("Encrypted password: " + parolaCriptata);

			String parolaDecriptata = decryptPassword(parolaCriptata);

			// convert from Hexa String format to normal string
			byte[] bytes;
			try {
				bytes = Hex.decodeHex(parolaDecriptata.toCharArray());
				System.out.println("Decrypted password: "
						+ new String(bytes, "UTF-8"));

			} catch (DecoderException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (pubStream != null) {
				pubStream.close();
				if (pubStream != null) {
					pubStream.close();
				}
			}

			if (privStream != null) {
				privStream.close();
				if (privStream != null) {
					privStream.close();
				}
			}
		}

	}

	// Modular Exponentiation
	// Square and Multiply - Right to left variant for y = a to the power of x
	// (mod p)
	// where y = encrypted password, x = public exponent, p = public modulus

	public static String encryptPassword(String password)
			throws UnsupportedEncodingException {
		BigInteger a = new BigInteger(password.getBytes("UTF-8"));

		BigInteger p = new BigInteger(publicModulus.toString(16), 16);
		BigInteger x = publicExponent;

		BigInteger y = BigInteger.ONE;
		while (x.compareTo(BigInteger.ZERO) > 0) {
			if (x.testBit(0))
				y = (y.multiply(a)).mod(p);
			x = x.shiftRight(1);
			a = (a.multiply(a)).mod(p);
		}
		return y.mod(p).toString(16);
	}

	public static String decryptPassword(String cipherPassword)
			throws UnsupportedEncodingException {
		BigInteger a = new BigInteger(cipherPassword, 16);
		BigInteger p = new BigInteger(publicModulus.toString(16), 16);
		BigInteger x = privExponent;

		BigInteger y = BigInteger.ONE;
		while (x.compareTo(BigInteger.ZERO) > 0) {
			if (x.testBit(0))
				y = (y.multiply(a)).mod(p);
			x = x.shiftRight(1);
			a = (a.multiply(a)).mod(p);
		}
		return y.mod(p).toString(16);
	}

}
