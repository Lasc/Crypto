import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

/**
 * 
 * @author Mihai Lasc
 * 
 */
public class MihaiLasc {

	/*salt and byte were randomly generated using java.security.SecureRandom 
	 * and hardcoded here because they can't be generated every time this code
	 * runs as they need to be constant for submission vie email in their
	 * hexadecimal format
	 */
	private static byte[] IV = { 106, -103, 76, 20, 13, -118, -80, 78, 42, -25,
			-90, 10, 99, -61, -110, 123 };
	private static byte[] salt = { -66, -118, 54, -70, 6, -84, 120, -3, 89,
			-107, 82, 101, 30, 120, 61, -19 };

	/* to convert the salt and vector from byte array to hexadecimal 
	 * I used the org.apache.commons.codec.binary.Hex.encodeHexString
	 * and again hardcoded here the hexa string for the same reasons
	 * as above  
	 */
	private static String IV_Hex = "6a994c140d8ab04e2ae7a60a63c3927b";
	private static String salt_Hex = "be8a36ba06ac78fd599552651e783ded";

	private static final String publicModulus = "c406136c12640a665900a9df4df63a84fc8559"
			+ "27b729a3a106fb3f379e8e4190ebba442f67b93402e535b18a5777e6490e67dbee95"
			+ "4bb02175e43b6481e7563d3f9ff338f07950d1553ee6c343d3f8148f71b4d2df8da7"
			+ "efb39f846ac07c865201fbb35ea4d71dc5f858d9d41aaa856d50dc2d2732582f80e7d38c32aba87ba9";

	private static final String publicExponent = "65537";

	public static void main(String[] args) throws UnsupportedEncodingException {
		System.out.println("To run the Jar: \n"
				+ "1: To Encrypt using the command line: \n"
				+ "java -jar MihaiLasc.jar <Password(String)> <File Name to be encrypted (String)>\n"
				+ "1: To Decrypt using the command line: \n"
				+ "java -jar MihaiLasc.jar <Password(String)> <Encrypted Zip file(HexaString)> "
				+ "<Aribitray String (used only to differentiate between encryption and decryption)>\n"
					);
		if (args.length == 2) {
			
			String password = args[0];
			byte[] AES_Key = null;
			try {
				// generate the AES key by apply SHA 200 times
				AES_Key = applySHA_256(password, salt);
				
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			// get the file name that is to be encrypted 
			String file = args[1];

			System.out.println("Your password is: " + password);
			System.out.println("Your encrypted password is: "
					+ rsaPassword(password) + "\n");

			System.out.println(salt.length * 8 + " bit " + "Salt: " + salt_Hex);
			System.out.println(IV.length * 8 + " bit " + "IV " + IV_Hex + "\n");

			AESfileEncryption(AES_Key, IV, file);
		}
		else if (args.length == 3)
		{
			// Additional execution Branch used to test 
			// AES decryption of the zipped encrypted file 

			String password = args[0]; // password in clear
			byte[] AES_Key = null;
			// regenerate the same way the AES encryption key
			try {
				AES_Key = applySHA_256(password, salt);
				System.out.println("AES Key is: "+AES_Key);
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}			
			String file = args[1];
			// Decrypt the zipped encrypted file that comes in here 
			// not as a file as it did for encryption
			// but as a Hexa String generated from that file
			// as per assignment requirements
			AESfileDecrypt(AES_Key, IV, file);
		}
		else
		{
			System.out.println("Please run this jar with suggested parameter order and type.");
		}
		
	}
	
	// hashing 200 times the password and salt using SHA-256
	public static byte[] applySHA_256(String password, byte[] salt)
			throws IOException, NoSuchAlgorithmException {
		
		byte[] password_salt = concatenate(password.getBytes("UTF-8"), salt);

		MessageDigest d = MessageDigest.getInstance("SHA-256");
		d.update(password_salt);

		byte[] hashedPassword = d.digest();
		for (int i = 0; i < 200; i++) {
			d.update(hashedPassword);
			hashedPassword = d.digest();
		}
		return hashedPassword;
	}

	private static byte[] concatenate(byte[] x, byte[] y) 
			throws IOException {
		
		ByteArrayOutputStream outputStream;
		outputStream = new ByteArrayOutputStream();
		outputStream.write(x);
		outputStream.write(y);

		return outputStream.toByteArray();
	}

	/* Modular Exponentiation
	  Square and Multiply - Right to left variant for:
	   		y = a to the power of x (mod p)
	   where y = encrypted password, x = public exponent, p = public modulus
	*/
	public static String rsaPassword(String password)
			throws UnsupportedEncodingException {
		BigInteger a = new BigInteger(password.getBytes("UTF-8"));
		BigInteger p = new BigInteger(publicModulus, 16);
		BigInteger x = new BigInteger(publicExponent);
		BigInteger y = BigInteger.ONE;
		
		while (x.compareTo(BigInteger.ZERO) > 0) {
			if (x.testBit(0))
				y = (y.multiply(a)).mod(p);
			x = x.shiftRight(1);
			a = (a.multiply(a)).mod(p);
		}
		return y.mod(p).toString(16);
	}

	public static void AESfileEncryption(byte[] key, byte[] IV, String file) {
			File clearFile = new File(file);
			byte[] fileData = new byte[(int) clearFile.length()];
			try
			{
				FileInputStream in = new FileInputStream(file);
				in.read(fileData);
				in.close();
				File encryptedFile = new File("Encrypted_" + file);

				// AES in CBC mode with the 256-bit key and NO Padding
				Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding", "SunJCE");
				cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(key, "AES"),
										new IvParameterSpec(IV));

				byte[] cipherText = null;
				try {
					// padding here through the second method in the lecture
					byte[] paddedData = paddPlainTextData(fileData);
					
					cipherText = cipher.doFinal(paddedData);

					FileOutputStream outputStream = new FileOutputStream(encryptedFile);
					outputStream.write(cipherText);
					System.out.println("Encrypted file in Hexadecimal: "+Hex.encodeHexString(cipherText));

					outputStream.flush();
					outputStream.close();
					System.out.println("Encrypted file produced: "
							+ encryptedFile.getAbsolutePath());

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
			
			catch (FileNotFoundException e)
			{
				System.out.println("Could not find the file passed in as argument: "+ file);
				System.out.println("Please place the file "+ file+ " in the current folder.");
			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (NoSuchAlgorithmException e1) {
				e1.printStackTrace();
			} catch (NoSuchProviderException e1) {
				e1.printStackTrace();
			} catch (NoSuchPaddingException e1) {
				e1.printStackTrace();
			} catch (InvalidKeyException e1) {
				e1.printStackTrace();
			} catch (InvalidAlgorithmParameterException e1) {
				e1.printStackTrace();
			}
	}

	// implementation of padding method 2 of ISO/IEC 9797-1
	// same as in the lecture
	private static byte[] paddPlainTextData(byte[] fileData) {
		int paddedDataLen = fileData.length + 1;
		if ((fileData.length + 1) % 16 != 0) {
			paddedDataLen += 16 - (fileData.length + 1) % 16;
		}

		byte[] paddedData = new byte[paddedDataLen];
		System.arraycopy(fileData, 0, paddedData, 0, fileData.length);

		paddedData[fileData.length] = (byte) 0x80;

		return paddedData;
	}
	
	public static void AESfileDecrypt(byte[] key, byte[] IV, String hexEncryptedZipFile) {
		byte[] fileData = null;
		try {
			fileData = Hex.decodeHex(hexEncryptedZipFile.toCharArray());
		} catch (DecoderException e2) {
			e2.printStackTrace();
		}
		try
		{
			File decryptedZipFile = new File("DecryptedZipFile.zip");

			// Decrypting with AES in CBC mode with the 256-bit key and NO Padding
			Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding", "SunJCE");
			cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(key, "AES"),
									new IvParameterSpec(IV));

			byte[] clearZip = null;
			try {

				clearZip = cipher.doFinal(fileData);

				FileOutputStream outputStream = new FileOutputStream(decryptedZipFile);
				outputStream.write(clearZip);

				outputStream.flush();
				outputStream.close();
				System.out.println("Decrypted file produced: "
						+ decryptedZipFile.getAbsolutePath());

			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		
		catch (NoSuchAlgorithmException e1) {
			e1.printStackTrace();
		} catch (NoSuchProviderException e1) {
			e1.printStackTrace();
		} catch (NoSuchPaddingException e1) {
			e1.printStackTrace();
		} catch (InvalidKeyException e1) {
			e1.printStackTrace();
		} catch (InvalidAlgorithmParameterException e1) {
			e1.printStackTrace();
		}
	}
}
