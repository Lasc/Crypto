

import java.math.BigInteger;


class Properties {

//    private static final String publicModulus = "c406136c12640a665900a9df4df63a84fc8559" +
//            "27b729a3a106fb3f379e8e4190ebba442f67b93402e535b18a5777e6490e67dbee95" +
//            "4bb02175e43b6481e7563d3f9ff338f07950d1553ee6c343d3f8148f71b4d2df8da7" +
//            "efb39f846ac07c865201fbb35ea4d71dc5f858d9d41aaa856d50dc2d2732582f80e7d38c32aba87ba9";
	
	private static final String publicModulus = "30819f300d06092a864886f70d010101050003818d00308189028181009d87618d0eb2b353dc944ba454c994ddad2b7fb0ab950535ba449b7c1e7d073249ec179cbb60eafbffeb872059a1bf80aafc600ececca224ee579750a862ae3f5dac01a92522a574f5b964bb5ae030295d619821c7d39ee81bbbae2cd820d0681fd7e896241bcc7633b52aed4644917db6aa9f374ddfeeafe3d02cc8bc7259010203010001";
    private static final String publicExponent = "65537";

    public static BigInteger getModulus() {
        return new BigInteger(publicModulus, 16);
    }

    public static BigInteger getExponent() {
        return new BigInteger(publicExponent);
    }
}
